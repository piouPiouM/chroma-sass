# -*- encoding: utf-8 -*-

require './lib/chroma_sass'

Gem::Specification.new do |s|
  s.version       = ChromaSass::VERSION
  s.date          = ChromaSass::DATE

  s.name          = "chroma_sass"
  s.authors       = ["Mehdi Kabab"]
  s.email         = ["pioupioum@gmail.com"]
  s.summary       = %q{Easily manage chromatic pages with Sass}
  s.homepage      = "https://github.com/piouPiouM/chroma_sass"
  s.license       = "MIT"

  s.files  = ["README.md", "LICENSE.txt"]
  s.files += Dir.glob("lib/**/*.*")
  s.files += Dir.glob("stylesheets/**/*.*")

  s.add_dependency("sass", ["~>3.3"])

  s.add_development_dependency "bundler", "~> 1.5"
  s.add_development_dependency "rake"
end


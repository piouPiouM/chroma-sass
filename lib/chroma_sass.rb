base_directory  = File.expand_path(File.join(File.dirname(__FILE__), '..'))
chroma_stylesheets_path = File.join(base_directory, 'stylesheets')

if (defined? Compass)
  Compass::Frameworks.register('chroma_sass', :path => base_directory)
else
  if ENV.has_key?('SASS_PATH')
    ENV['SASS_PATH'] = ENV['SASS_PATH'] + File::PATH_SEPARATOR + chroma_stylesheets_path
  else
    ENV['SASS_PATH'] = chroma_stylesheets_path
  end
end

module ChromaSass
  VERSION = '1.0.0.pre'
  DATE = '2016-05-13'
end

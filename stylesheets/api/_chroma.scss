////
/// Public mixins and functions for ChromaSass.
/// @group API
////

/// Returns the color associated with a color variant in a palette.
///
/// @param {String} $palette - Name of the palette to lookup. 
/// @param {String} $color-variant [$ppm-chroma-default-key] - Name of the color to retrieve.
///
/// @return {Map}
///
/// @see ppm-chroma-addPalette()
@function ppm-chroma-getColorVariant($palette, $color-variant: $ppm-chroma-default-key) {
  @return map-get(_ppm-chroma-getPalette($palette), $color-variant);
}

/// Declare the properties to decline in multiple colors.
///
/// @param {Arglist} $properties
///
/// @example scss - Usage
///   @include ppm-chroma-declareProperties(
///     'color', 'background-color'
///   );
@mixin ppm-chroma-declareProperties($properties...) {
  @each $property in $properties {
    $propertyCamel: ppm-string-camelize($property, 'lower');
    $ppm-chroma-properties: append($ppm-chroma-properties, $property) !global;
    $_ppm-chroma-propertyCamelMap: map-merge($_ppm-chroma-propertyCamelMap, ($property: $propertyCamel)) !global;
  }
}

/// Declare a new palette.
///
/// @param {String} $name - Name of the palette to declare.
/// @param {Map} $color-stops [()] - Map of the colors.
///
/// @prop {String} base - Default palette's color.
///
/// @example scss - Usage
///   @include ppm-chroma-addPalette('pink', (
///     base:  #FF33CC,
///     dark:  #B2248F,
///     light: #FF5CD6,
///   ));
///
/// @throw No 'base' key was found in palette '#{$name}'.
/// @require $ppm-chroma
/// @require $ppm-chroma-properties
@mixin ppm-chroma-addPalette($name, $color-stops: ()) {
  @if not map-has-key($color-stops, $ppm-chroma-default-key) {
    @error "No 'base' key was found in palette '#{$name}'.";
  }

  @each $color-variant in map-keys($color-stops) {
    $variant: _ppm-chroma-formatColorVariant($color-variant);
    $_ppm-chroma-variantFormatedMap: map-merge($_ppm-chroma-variantFormatedMap, ($color-variant: $variant)) !global;
  }

  $ppm-chroma: map-merge($ppm-chroma, ($name: $color-stops)) !global;
}

/// Generates the placeholders.
///
/// @example scss - Usage
///   // In project's configuration.
///   @include ppm-chroma-declareProperties(
///     'color', 'background-color'
///   );
///   @include ppm-chroma-addPalette('pink', (
///     base:  #FF33CC,
///     dark:  #B2248F,
///     light: #FF5CD6,
///   ));
///   @include ppm-chroma-addPalette('blue', (
///     base:  #0066FF,
///     dark:  #003D99,
///     light: #4D94FF,
///   ));
///   
///   // In project main file.
///   @include ppm-chroma-create;
///
/// @example scss - Generated placeholders
///   // In project main file.
///   %theme-pink-color                     { color: #FF33CC; }
///   %theme-pink-color--dark               { color: #B2248F; }
///   %theme-pink-color--light              { color: #FF5CD6; }
///   %theme-pink-color--linkVisited        { color: #FF33CC; }
///   %theme-pink-color--linkHover          { color: #FF33CC; }
///   %theme-pink-color--linkFocus          { color: #FF33CC; }
///   %theme-pink-color--linkActive         { color: #FF33CC; }
///   %theme-pink-color--linkVisited--dark  { color: #B2248F; }
///   %theme-pink-color--linkHover--dark    { color: #B2248F; }
///   %theme-pink-color--linkFocus--dark    { color: #B2248F; }
///   %theme-pink-color--linkActive--dark   { color: #B2248F; }
///   %theme-pink-color--linkVisited--light { color: #FF5CD6; }
///   %theme-pink-color--linkHover--light   { color: #FF5CD6; }
///   %theme-pink-color--linkFocus--light   { color: #FF5CD6; }
///   %theme-pink-color--linkActive--light  { color: #FF5CD6; }
///   %theme-pink-backgroundColor                     { background-color: #FF33CC; }
///   %theme-pink-backgroundColor--dark               { background-color: #B2248F; }
///   %theme-pink-backgroundColor--light              { background-color: #FF5CD6; }
///   %theme-pink-backgroundColor--linkVisited        { background-color: #FF33CC; }
///   %theme-pink-backgroundColor--linkHover          { background-color: #FF33CC; }
///   %theme-pink-backgroundColor--linkFocus          { background-color: #FF33CC; }
///   %theme-pink-backgroundColor--linkActive         { background-color: #FF33CC; }
///   %theme-pink-backgroundColor--linkVisited--dark  { background-color: #B2248F; }
///   %theme-pink-backgroundColor--linkHover--dark    { background-color: #B2248F; }
///   %theme-pink-backgroundColor--linkFocus--dark    { background-color: #B2248F; }
///   %theme-pink-backgroundColor--linkActive--dark   { background-color: #B2248F; }
///   %theme-pink-backgroundColor--linkVisited--light { background-color: #FF5CD6; }
///   %theme-pink-backgroundColor--linkHover--light   { background-color: #FF5CD6; }
///   %theme-pink-backgroundColor--linkFocus--light   { background-color: #FF5CD6; }
///   %theme-pink-backgroundColor--linkActive--light  { background-color: #FF5CD6; }
///   .t-pink {
///     %theme-color {
///       @extend %theme-pink-color;
///     }
///     &%theme-color--itself {
///       @extend %theme-pink-color;
///     }
///     %theme-color--linkVisited        { @extend %theme-pink-color--linkVisited; }
///     %theme-color--linkHover          { @extend %theme-pink-color--linkHover; }
///     %theme-color--linkFocus          { @extend %theme-pink-color--linkFocus; }
///     %theme-color--linkActive         { @extend %theme-pink-color--linkActive; }
///     %theme-color--linkVisited--dark  { @extend %theme-pink-color--linkVisited--dark; }
///     %theme-color--linkHover--dark    { @extend %theme-pink-color--linkHover--dark; }
///     %theme-color--linkFocus--dark    { @extend %theme-pink-color--linkFocus--dark; }
///     %theme-color--linkActive--dark   { @extend %theme-pink-color--linkActive--dark; }
///     %theme-color--linkVisited--light { @extend %theme-pink-color--linkVisited--light; }
///     %theme-color--linkHover--light   { @extend %theme-pink-color--linkHover--light; }
///     %theme-color--linkFocus--light   { @extend %theme-pink-color--linkFocus--light; }
///     %theme-color--linkActive--light  { @extend %theme-pink-color--linkActive--light; }
///     %theme-backgroundColor {
///       @extend %theme-pink-backgroundColor;
///     }
///     &%theme-backgroundColor--itself {
///       @extend %theme-pink-backgroundColor;
///     }
///     %theme-backgroundColor--linkVisited        { @extend %theme-pink-backgroundColor--linkVisited; }
///     %theme-backgroundColor--linkHover          { @extend %theme-pink-backgroundColor--linkHover; }
///     %theme-backgroundColor--linkFocus          { @extend %theme-pink-backgroundColor--linkFocus; }
///     %theme-backgroundColor--linkActive         { @extend %theme-pink-backgroundColor--linkActive; }
///     %theme-backgroundColor--linkVisited--dark  { @extend %theme-pink-backgroundColor--linkVisited--dark; }
///     %theme-backgroundColor--linkHover--dark    { @extend %theme-pink-backgroundColor--linkHover--dark; }
///     %theme-backgroundColor--linkFocus--dark    { @extend %theme-pink-backgroundColor--linkFocus--dark; }
///     %theme-backgroundColor--linkActive--dark   { @extend %theme-pink-backgroundColor--linkActive--dark; }
///     %theme-backgroundColor--linkVisited--light { @extend %theme-pink-backgroundColor--linkVisited--light; }
///     %theme-backgroundColor--linkHover--light   { @extend %theme-pink-backgroundColor--linkHover--light; }
///     %theme-backgroundColor--linkFocus--light   { @extend %theme-pink-backgroundColor--linkFocus--light; }
///     %theme-backgroundColor--linkActive--light  { @extend %theme-pink-backgroundColor--linkActive--light; }
///   }
///   
///   %theme-blue-color                     { color: #0066FF; }
///   %theme-blue-color--dark               { color: #003D99; }
///   %theme-blue-color--light              { color: #4D94FF; }
///   %theme-blue-color--linkVisited        { color: #0066FF; }
///   %theme-blue-color--linkHover          { color: #0066FF; }
///   %theme-blue-color--linkFocus          { color: #0066FF; }
///   %theme-blue-color--linkActive         { color: #0066FF; }
///   %theme-blue-color--linkVisited--dark  { color: #003D99; }
///   %theme-blue-color--linkHover--dark    { color: #003D99; }
///   %theme-blue-color--linkFocus--dark    { color: #003D99; }
///   %theme-blue-color--linkActive--dark   { color: #003D99; }
///   %theme-blue-color--linkVisited--light { color: #4D94FF; }
///   %theme-blue-color--linkHover--light   { color: #4D94FF; }
///   %theme-blue-color--linkFocus--light   { color: #4D94FF; }
///   %theme-blue-color--linkActive--light  { color: #4D94FF; }
///   %theme-blue-backgroundColor                     { background-color: #0066FF; }
///   %theme-blue-backgroundColor--dark               { background-color: #003D99; }
///   %theme-blue-backgroundColor--light              { background-color: #4D94FF; }
///   %theme-blue-backgroundColor--linkVisited        { background-color: #0066FF; }
///   %theme-blue-backgroundColor--linkHover          { background-color: #0066FF; }
///   %theme-blue-backgroundColor--linkFocus          { background-color: #0066FF; }
///   %theme-blue-backgroundColor--linkActive         { background-color: #0066FF; }
///   %theme-blue-backgroundColor--linkVisited--dark  { background-color: #003D99; }
///   %theme-blue-backgroundColor--linkHover--dark    { background-color: #003D99; }
///   %theme-blue-backgroundColor--linkFocus--dark    { background-color: #003D99; }
///   %theme-blue-backgroundColor--linkActive--dark   { background-color: #003D99; }
///   %theme-blue-backgroundColor--linkVisited--light { background-color: #4D94FF; }
///   %theme-blue-backgroundColor--linkHover--light   { background-color: #4D94FF; }
///   %theme-blue-backgroundColor--linkFocus--light   { background-color: #4D94FF; }
///   %theme-blue-backgroundColor--linkActive--light  { background-color: #4D94FF; }
///   .t-blue {
///     %theme-color {
///       @extend %theme-blue-color;
///     }
///     &%theme-color--itself {
///       @extend %theme-blue-color;
///     }
///     %theme-color--linkVisited        { @extend %theme-blue-color--linkVisited; }
///     %theme-color--linkHover          { @extend %theme-blue-color--linkHover; }
///     %theme-color--linkFocus          { @extend %theme-blue-color--linkFocus; }
///     %theme-color--linkActive         { @extend %theme-blue-color--linkActive; }
///     %theme-color--linkVisited--dark  { @extend %theme-blue-color--linkVisited--dark; }
///     %theme-color--linkHover--dark    { @extend %theme-blue-color--linkHover--dark; }
///     %theme-color--linkFocus--dark    { @extend %theme-blue-color--linkFocus--dark; }
///     %theme-color--linkActive--dark   { @extend %theme-blue-color--linkActive--dark; }
///     %theme-color--linkVisited--light { @extend %theme-blue-color--linkVisited--light; }
///     %theme-color--linkHover--light   { @extend %theme-blue-color--linkHover--light; }
///     %theme-color--linkFocus--light   { @extend %theme-blue-color--linkFocus--light; }
///     %theme-color--linkActive--light  { @extend %theme-blue-color--linkActive--light; }
///     %theme-backgroundColor {
///       @extend %theme-blue-backgroundColor;
///     }
///     &%theme-backgroundColor--itself {
///       @extend %theme-blue-backgroundColor;
///     }
///     %theme-backgroundColor--linkVisited        { @extend %theme-blue-backgroundColor--linkVisited; }
///     %theme-backgroundColor--linkHover          { @extend %theme-blue-backgroundColor--linkHover; }
///     %theme-backgroundColor--linkFocus          { @extend %theme-blue-backgroundColor--linkFocus; }
///     %theme-backgroundColor--linkActive         { @extend %theme-blue-backgroundColor--linkActive; }
///     %theme-backgroundColor--linkVisited--dark  { @extend %theme-blue-backgroundColor--linkVisited--dark; }
///     %theme-backgroundColor--linkHover--dark    { @extend %theme-blue-backgroundColor--linkHover--dark; }
///     %theme-backgroundColor--linkFocus--dark    { @extend %theme-blue-backgroundColor--linkFocus--dark; }
///     %theme-backgroundColor--linkActive--dark   { @extend %theme-blue-backgroundColor--linkActive--dark; }
///     %theme-backgroundColor--linkVisited--light { @extend %theme-blue-backgroundColor--linkVisited--light; }
///     %theme-backgroundColor--linkHover--light   { @extend %theme-blue-backgroundColor--linkHover--light; }
///     %theme-backgroundColor--linkFocus--light   { @extend %theme-blue-backgroundColor--linkFocus--light; }
///     %theme-backgroundColor--linkActive--light  { @extend %theme-blue-backgroundColor--linkActive--light; }
///   }
///   
///   %theme-desactivate {
///     color: inherit;
///     background-color: inherit;
///   }
///
/// @throw You must declare at least one palette with the mixin `ppm-chroma-addPalette()`.
/// @throw You must declare at least one property with the mixin `ppm-chroma-declareProperties()`.
/// @require $ppm-chroma
/// @require $ppm-chroma-properties
@mixin ppm-chroma-create() {
  @if 0 == length($ppm-chroma) {
    @error "You must declare at least one palette with the mixin `ppm-chroma-addPalette()`.";
  }

  @if 0 == length($ppm-chroma-properties) {
    @error "You must declare at least one property with the mixin `ppm-chroma-declareProperties()`.";
  }

  @include _ppm-chroma-createFinalPlaceholders;
  @include _ppm-chroma-createFinalPlaceholdersStates;

  @each $palette, $color-stops in $ppm-chroma {
    .#{$ppm-chroma-classPrefix}#{$palette} {
      @each $property in $ppm-chroma-properties {
        @each $color-variant in map-keys($color-stops) {
          #{_ppm-chroma-getPlaceholder($property, $color-variant)} {
            @include _ppm-chroma-extendVariant($property, $color-variant, $palette);
          }
          @at-root &#{_ppm-chroma-getPlaceholder($property, $color-variant, $extra: $ppm-chroma-itSelfSuffix)} {
            @include _ppm-chroma-extendVariant($property, $color-variant, $palette);
          }
        }
      }

      @if $ppm-chroma-support-links {
        @each $property in $ppm-chroma-properties {
          @each $color-variant in map-keys($color-stops) {
            @each $state in $ppm-chroma-links-states {
              #{_ppm-chroma-getPlaceholder($property, $color-variant, $extra: _ppm-chroma-formatState($state))} {
                @include _ppm-chroma-extendVariant($property, $color-variant, $palette, $extra: _ppm-chroma-formatState($state));
              }
            }
          }
        }
      }

      // %theme-desactivate
      %#{$ppm-chroma-placeholderPrefix}desactivate {
        @each $property in $ppm-chroma-properties {
          #{$property}: inherit;
        }
      } // %theme-desactivate
    }
  }
}

@mixin ppm-chroma($properties, $color-variant: $ppm-chroma-default-key) {
  @include _ppm-chroma-extendVariant($properties, $color-variant);
}

@mixin ppm-chroma-itSelf($properties, $color-variant: $ppm-chroma-default-key, $extra: $ppm-chroma-itSelfSuffix) {
  @include _ppm-chroma-extendVariant($properties, $color-variant, $extra: $extra);
}

